[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]
[{assign var="shippinggroups" value=$oView->getShippingGroups()}]

[{*$shippinggroups|var_dump}]
[{$shippinggroups[0]|var_dump}]
                            [{foreach from=$shippinggroups item="shipkey"}]
                                [{$shipkey}]
                            [{/foreach*}]
<div class="container-fluid" style="text-align:center;margin-bottom:5px">
    <img style="width:20%" src="../modules/ewald/real/out/img/real.png">
</div>
<div class="container-fluid" >
    <div class="row">
        <div class="col-8">
            <div class="card bg-light">
                <div class="card-body">
                    <div class="input-group" style="width:25%;float:left">
                        <input name="ean" type="text" class="form-control" placeholder="Hier EAN eingeben..." aria-label="ean" >
                    </div>
                    <button type="button" style="height:32px" class="btn btn-outline-success" onclick="offerEanSearch($('input[name=\'ean\']').val())">Suchen</button>
                    <table class="table table-striped">
                        <thead>
                        <th>IDItem</th>
                        <th>Zustand</th>
                        <th>Menge</th>
                        <th>Preis</th>
                        <th>Title</th>
                        <th>EAN</th>
                        <th>Valide</th>
                        <th>Eingestellt am</th>
                        <th>Lieferart</th>
                        </thead>
                        <tbody id="tableOffers">
                        [{if $units}]
                            [{foreach from=$units item="key"}]
                                <tr style="cursor:pointer" onclick="showDetails('[{$key->id_unit}]','[{$key->itemtitle}]','[{$key->amount}]','[{$key->price}]','[{$key->deltimemin}]','[{$key->deltimemax}]','[{$key->shippinggroup}]','[{$key->shippingrate}]')">
                                    <td>[{$key->id_item}]</td>
                                    <td>[{$key->condition}]</td>
                                    <td>[{$key->amount}]</td>
                                    <td>[{$key->price}] €</td>
                                    <td>[{$key->itemtitle}]</td>
                                    <td>[{$key->itemean}]</td>
                                    <td>[{$key->itemvalid}]</td>
                                    <td>[{$key->iteminsert}]</td>
                                    <td>[{$key->shippinggroup}]</td>
                                </tr>
                            [{/foreach}]
                        [{/if}]
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card bg-light">
                <div class="card-header">
                    <h2>Angebotsdaten aktualisieren</h2>
                </div>
                <div class="card-body card-product-data">
                    <input type="hidden" name="id_unit" value="">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="title">Titel</span>
                        </div>
                        <input disabled name="title" type="text" class="form-control" placeholder="Titel" aria-label="Titel" >
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="Menge">Menge</span>
                        </div>
                        <input required name="amount" type="text" class="form-control" placeholder="Menge" aria-label="Menge" >
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="price">Preis</span>
                        </div>
                        <input required name="price" type="text" class="form-control" placeholder="Preis" aria-label="Preis" >
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="delivery_time_min">Min. Lieferzeit</span>
                        </div>
                        <input required name="delivery_time_min" type="text" class="form-control" placeholder="Minimale Lieferzeit" aria-label="Minimale Lieferzeit" >
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="delivery_time_max">Max. Lieferzeit</span>
                        </div>
                        <input required name="delivery_time_max" type="text" class="form-control" placeholder="Maximale Lieferzeit" aria-label="Maximale Lieferzeit" >
                    </div>
                    [{*
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="shipping_group">Lieferart</span>
                        </div>

                        <select name="shipping_group" aria-label="Lieferart" class="form-control">
                            [{foreach from=$shippinggroups item="shipkey"}]
                                <option>[{$shipkey}]</option>
                            [{/foreach}]
                        </select>
                    </div>
                    *}]
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="shipping_group">Lieferkosten</span>
                        </div>
                        <input disabled name="shipping_rate" type="text" class="form-control" placeholder="Lieferkosten" aria-label="Lieferkosten" >
                    </div>

                    <button type="button" class="btn btn-info" onclick="updateUnit()">Aktualisieren</button>


                </div>
            </div>
        </div>

    </div>
</div>
