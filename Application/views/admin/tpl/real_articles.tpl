[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]

<div class="container-fluid" style="text-align:center;margin-bottom:5px" onclick="test()">
    <img style="width:20%" src="../modules/ewald/real/out/img/real.png">
</div>
[{*<div style="width:25%; margin-left:15px;">
    <div class="card bg-light">
        <div class="card-header">
            <label for="sucheReal">Realsuche:</label>
            <input type="text" name="sucheReal" value="">
            <button type="button" class="btn btn-outline-info float-right" onclick="ArtikelSucheReal()">Suchen</button>
        </div>
    </div>
</div>*}]
<div class="container-fluid" >
        <div class="row">
            <div class="col">
                <div class="card bg-light">
    <form action="" method="post">
        <input type="hidden" name="fnc" value="getArticles">
        <input type="hidden" name="additionalAttributes" value="">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-3">
                                <h2>Artikel</h2>
                            </div>
                            <div class="col-9">
                                <div class="float-right">
                                    <label for="suche">Suche:</label>
                                    <input id="suche" type="text" name="suche" value="">
                                    <button type="submit" class="btn-xs btn-outline-success">Absenden</button>
                                </div>
                            </div>
                        </div>

                    </div>
    </form>
                    <div class="card-body">

                        [{if $curl}]
                        <table class="table table-striped">
                            <thead>
                                <th>OXID</th>
                                <th>Artikelnummer</th>
                                <th>Titel</th>
                                <th></th>
                            </thead>
                            <tbody>
                            [{foreach from=$curl->response->docs item=key}]
                                <tr class="parentRow" id="tr[{$key->id|replace:'.':'_'}]">

                                    <td>[{$key->id}]</td>
                                    <td>[{$key->oxartnum}]</td>
                                    <td>[{$key->oxtitle_txt_de}]</td>
                                    <td>[{if $key->oxartnum neq ($key->childartnums_txt[0]|strip:"")}] <i id="getChildren[{$key->id|replace:'.':'_'}]" style="cursor:pointer" onclick="getChildren('[{$key->id}]')" class="fa fa-caret-down fa-2x" aria-hidden="true"></i>
                                        [{else}]<i style="cursor:pointer" onclick="getArticle('[{$key->id}]')" class="fa fa-arrow-right fa-2x" aria-hidden="true"></i>
                                        [{/if}]</td>
                                </tr>
                            [{/foreach}]
                            </tbody>
                        </table>

                        [{/if}]

                        [{*if $erg}]
                        <table class="table table-striped">
                            <th>OXID</th>
                            <th>Artikelnummer</th>
                            <th>Titel</th>
                            <th>Preis</th>
                        </table>
                        <pre>
                            [{$erg|print_r}]
                        [{/if*}]

                    </div>
                </div>
            </div>
            <div class="col" id="productData">
                <div class="card bg-light">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6" style="white-space: nowrap">
                                <h2 id="CardHeaderCreate">Produktdaten anlegen/updaten</h2>
                                <i id="refreshArticle" style="display:none;cursor:pointer" data-toggle="tooltip" data-placement="right" title="Neu laden" class="fa fa-sync fa-2x" onclick="getArticle()"></i>
                            </div>
                            <div class="col-6">
                                <div id="realExisting" class="alert alert-danger float-right" role="alert" style="display:none">
                                    Dieser Artikel ist bereits bei REAL angelegt! <span id="updateStatus"></span>
                                </div>
                                <div id="realNotExisting" class="alert alert-success float-right" role="alert" style="display:none">
                                    Dieser Artikel ist noch nicht bei REAL angelegt!
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body card-product-data">
                        [{*<form method="post" action="">*}]
                            <input type="hidden" name="fnc" value="createArticle">
                            <input type="hidden" name="oxid">
                            <input type="hidden" name="update" value="0">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="ean">EAN</span>
                                </div>
                                <input required name="ean" type="text" class="form-control" placeholder="EAN" aria-label="EAN">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="title">Titel</span>
                                </div>
                                <input required name="title" type="text" class="form-control" placeholder="Titel" aria-label="Titel" >
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="category">Kategorie</span>
                                </div>
                                <input required name="category" type="text" class="form-control" placeholder="Kategorie" aria-label="Kategorie">
                                <button type="button" class="btn btn-outline-info" data-toggle="tooltip" data-placement="left" title="Kategorie vorschlagen" onclick="guessCategory()">
                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="description">Beschreibung</span>
                                </div>
                                <textarea required name="description" class="form-control" placeholder="Beschreibung" aria-label="Beschreibung"></textarea>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="short_description">Keywords</span>
                                </div>
                                <textarea required name="short_description" class="form-control" placeholder="Keywords" aria-label="Keywords"></textarea>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="mpn">Herstellernummer</span>
                                </div>
                                <input required name="mpn" type="text" class="form-control" placeholder="Herstellernummer(Artikelnummer)" aria-label="Kategorie">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="manufacturer">Hersteller</span>
                                </div>
                                <input required name="manufacturer" type="text" class="form-control" placeholder="Hersteller" aria-label="Kategorie">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="list_price">UVP in &euro;</span>
                                </div>
                                <input name="list_price" type="number" min="0.00" max="100000.00" step="0.01" class="form-control" placeholder="Preis" aria-label="Preis">
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="list_price">Bild</span>
                                </div>
                                <input required name="picture" type="text" class="form-control" placeholder="Bild" aria-label="Bild">
                            </div>
                        <div id="pflichtAttribute"></div>
                        <div id="optAttribute"></div>

                            <button id="updateCreate" type="button" class="btn btn-outline-info float-right" onclick="createArticle()">Anlegen/Update</button>
                        [{*</form>*}]
                        <button id="deleteProduct" style="display:none" type="button" onclick="deleteProduct()" class="btn btn-outline-danger float-left hidden" data-toggle="tooltip" data-placement="left" title="Produkt löschen"><i class="fa fa-trash"></i></button>
                        <button id="sellProduct" style="display:none" type="button" onclick="$('#unitModal').modal();" class="btn btn-outline-danger float-left hidden" data-toggle="tooltip" data-placement="left" title="Produkt anbieten">Anbieten</button>

                        [{* MODAL *}]
                        <div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Kategorievorschläge</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="testModalBody">

                                    </div>
                                    <div class="modal-footer" id="testModalFooter">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        [{* MODAL *}]

                        [{* MODAL *}]
                        <div class="modal fade" id="unitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="unitModalTitle">Angebot erstellen</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" id="unitModalBody">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="sellCondition">Zustand</span>
                                            </div>
                                        <select id="selectCondition" class="custom-select">
                                            <option selected>Neu</option>
                                            <option>Gebraucht - wie neu</option>
                                            <option>Gebraucht - sehr gut</option>
                                            <option>Gebraucht - gut</option>
                                            <option>Gebraucht - akzeptabel</option>
                                        </select>
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="sellPrice">Preis</span>
                                            </div>
                                            <input required name="sellPrice" type="text" class="form-control" placeholder="Preis" aria-label="Kategorie">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="sellBestand">Bestand</span>
                                            </div>
                                            <input required name="sellBestand" type="text" class="form-control" placeholder="Bestand" aria-label="Kategorie">
                                        </div>
                                        </div>
                                    <div class="modal-footer" id="unitModalFooter">
                                        <button type="button" class="btn btn-success" onclick="sellProductAJAX()">Anlegen</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        [{* MODAL *}]

                    </div>
            </div>
        </div>

</div>
