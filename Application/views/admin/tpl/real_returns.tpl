[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]

<div class="container-fluid" style="text-align:center;margin-bottom:5px">
    <img style="width:20%" src="../modules/ewald/real/out/img/real.png">
</div>
[{assign var="test" value=$oView->acceptReturn()}]
[{$test}]

<div class="container-fluid" >
    <div class="row">
        <div class="col-7">
            <div class="card bg-light">
                <div class="card-body">


                    <table class="table table-striped">
                        <caption>Retouren der letzten 30 Tage</caption>
                        <thead>
                            <th>IDRetoure</th>
                            <th>Erstellung</th>
                            <th>Updated</th>
                            <th>Tracking-Art</th>
                            <th>Tracking-Code</th>
                            <th>Status</th>
                        </thead>
                        <tbody>
                            [{if $units}]
                                [{foreach from=$units item="key"}]
                                    <tr style="cursor:pointer" onclick="getReturn('[{$key->id_return}]');">
                                        <td>[{$key->id_return}]</td>
                                        <td>[{$key->ts_created}]</td>
                                        <td>[{$key->ts_updated}]</td>
                                        <td>[{$key->tracking_provider}]</td>
                                        <td>[{$key->tracking_code}]</td>
                                        <td>[{$key->status}]</td>
                                    </tr>
                                [{/foreach}]
                            [{/if}]
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card bg-light">
                <div class="card-header">
                    <h2>RetourenDetails</h2>
                </div>
                <div class="card-body card-product-data">
                    <input type="hidden" name="id_unit" value="">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="title">Titel</span>
                        </div>
                        <input disabled name="title" type="text" class="form-control" placeholder="Titel" aria-label="Titel" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
