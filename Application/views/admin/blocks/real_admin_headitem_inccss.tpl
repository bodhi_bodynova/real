[{$smarty.block.parent}]

[{if $oView->getClassName() == 'real_articles' ||
    $oView->getClassName() == 'real_orders' ||
    $oView->getClassName() == 'real_offers' ||
    $oView->getClassName() == 'real_returns'}]
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<link rel="stylesheet" href="[{$oViewConf->getModuleUrl('real','out/src/css/bootstrap.css')}]">
<link rel="stylesheet" href="[{$oViewConf->getModuleUrl('real','out/src/css/real.css')}]">
<style>
    .card-product-data input{
        height:auto;
    }
    .card-product-data span{
        min-width:180px;
    }
    .card-product-data .input-group{
        margin-bottom:10px;
    }
    .parentRow{
        border-bottom-width: 2px;
        border-bottom-color: gray;
        border-bottom-style: solid;
        border-top-width: 2px;
        border-top-color: gray;
        border-top-style: solid;
    }

    ul li{
        background:none !important;
    }

    .list-group-item-cust {
        padding-left: 0px;
    }

    .list-group-addon {
        padding: 10px 10px;
        font-size: 15px;
        display:inline;
        font-weight: normal;
        line-height: 1;
        color: #555555;
        text-align: center;
        background-color: #eeeeee;
        border: 1px solid #cccccc;
        border-radius: 6px;
    }
    .background-white{
        background-color:white !important;
    }

</style>
[{/if}]

