[{$smarty.block.parent}]

[{if $oView->getClassName() == 'real_articles' ||
$oView->getClassName() == 'real_orders' ||
$oView->getClassName() == 'real_offers' ||
$oView->getClassName() == 'real_returns'}]
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript" src="[{$oViewConf->getModuleUrl('real','out/src/js/real.js')}]"></script>



<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


    function ArtikelSucheReal(param = null,rek = null){

        var suchparam;
        if(param){
            suchparam = param;
        } else {
            suchparam = $('input[name="sucheReal"]').val();
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=realSuche&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                suchparam: suchparam
            },
            success: function(result){
                console.log(result);

                /* RANKING
                var Ranking = ;
                for(var i = 0; i < result.length; i++){
                    if(Ranking[result[i].category.id_category] > 0){
                        Ranking[result[i].category.id_category] += 1;
                    } else {
                        Ranking[result[i].category.id_category] = 1;
                    }
                }
                console.log(Ranking);
                return;
                // RANKING */

                if(!result[0] || rek){
                    if(!rek){
                        var liste = '<p>Wähle aus folgender Liste oder benutze die Realsuche</p><ul id="listCat"></ul>';
                        $('#productData').fadeTo(10,0.5);
                        $('#testModalBody').append(liste);
                        //alert('');
                    }
                    if(result[0]){
                        if($('#'+result[0].category.id_category+'').length < 1){
                            var x = [];
                            x[0] = '\'' + result[0].category.name + '\'';
                            x[1] = '\'' + result[0].category.id_category + '\'';
                            $('#listCat').append('<li style="cursor:pointer" id="'+result[0].category.id_category+'" onclick="saveCategoryVorschlagAusListe('+x+')">'+result[0].category.name+'</li>');
                        }

                        //$('#testModalBody').append('<p>'+result[0].category.name+'</p>');
                    }
                    var lastIndex = suchparam.lastIndexOf(" ");

                    suchparam = suchparam.substring(0, lastIndex);
                    if(suchparam ===""){
                        $('#productData').fadeTo(10,1);
                        var realSuche = '            <label for="sucheReal">Realsuche:</label>' +
                            '            <input type="text" name="sucheReal" value="">' +
                            '            <button type="button" class="btn btn-outline-info float-right" onclick="ArtikelSucheReal()">Suchen</button>';
                        $('#testModalBody').append(realSuche);
                        $('#testModal').modal();
                        return;
                    }
                    $('input[name="sucheReal"]').val(suchparam);


                    ArtikelSucheReal(suchparam,1);
                } else{

                    //console.log(result);
                    console.log(result[0].category.name);
                    var suche = [];

                    suche[0] = result[0].category.name;
                    suche[1] = result[0].category.id_category;
                    saveCategoryVorschlag(suche);
                }
            },
            error: function(result){
                console.log(result + "\nERROR");
            }
        });
    }

    function createArticle(){

        var oxid = $('input[name="oxid"]').val();
        var ean = $('input[name="ean"]').val();
        var title = $('input[name="title"]').val();
        var category = $('input[name="category"]').val();
        var description = $('textarea[name="description"]').val();
        var short_description = $('textarea[name="short_description"]').val();
        var mpn = $('input[name="mpn"]').val();
        var picture = $('input[name="picture"]').val();
        var manufacturer = $('input[name="manufacturer"]').val();
        var material_composition = $('input[name="material_composition"]').val();
        var update = $('input[name="update"]').val();
        var additionalAttributes = $('input[name="additionalAttributes"]').val();



        var arrVal = [];

        for(var i = 0; i < additionalAttributes.split(',').length; i++){
        //    console.log(additionalAttributes[i]);
            arrVal.push($('input[name="'+additionalAttributes.split(',')[i]+'"]').val());
        }

        $.ajax({
            type: 'POST',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=createArticle&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                ean: ean,
                title: title,
                category: category,
                description: description,
                short_description: short_description,
                mpn: mpn,
                picture: picture,
                manufacturer: manufacturer,
                material_composition: material_composition,
                update: update,
                additionalAttributes: additionalAttributes,
                arrVal: arrVal
            },
            success: function(result){
                console.log(result);
                getArticle(oxid);
            },
            error: function(result){
                console.log(result + '\nERROR');
            }
        })
    }


    function getChildren(oxid){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getChildren&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                oxid: oxid
            },
            success: function(result){
                console.log(result);
                var res = result;
                var sComplete = '';
                var onclickHide = '';
                for(var i = 0;res[i] !== undefined; i++ ){
                    onclickHide+= '$("#tr'+res[i][0].replace(".","_")+'").hide();';
                    var arr = res[i];
                    var sTR = '<tr id="tr' + res[i][0].replace(".","_") + '">';

                    for(var j = 0; j<3; j++ ) {
                        if(j=== 0){
                            sTR += '<td style="white-space: nowrap"><i class="fa fa-arrow-right" style="color:green;"></i><i class="fa fa-arrow-right" style="color:green;"></i>'+arr[j]+'</td>'
                        } else if(j===2){
                            var status = null;
                            if((status = arr[4].update_status) === undefined){
                                status = 'Nicht angelegt!';
                            }
                            var hint = '';
                            var style = '';
                            if(arr[4].item_ready === false){
                                hint = arr[4].item_not_ready_reason;
                                style= 'style="cursor:default;"';
                            } else {
                                hint = ''
                            }
                            sTR += '<td ' + style + ' data-toggle="tooltip" data-placement="top" title="'+hint+'">Uploadstatus: '+status+'</td>'
                        }
                        else {
                            sTR += '<td>'+arr[j]+'</td>'
                        }
                    }

                    sTR += '<td style="cursor:pointer" onclick="getArticle(\''+res[i][0]+'\')"><i class="fa fa-arrow-right fa-2x"></i></td></tr>';

                    sComplete += sTR;
                }



                console.log(sComplete);
                console.log(onclickHide);
                //$('#tr'+oxid.replace(".","_")).append(sTR);
                $(sComplete).insertAfter('#tr'+oxid.replace(".","_"));

                /**
                 * Klickfunktion zum Ausklappen der Varianten ändern, damit diese wieder "eingeklappt" werden.
                 */
                onclickHide += '$("#getChildren'+oxid.replace(".","_")+'").attr(\'class\',\'fa fa-caret-down fa-2x\');';
                onclickHide += '$("#getChildren'+oxid.replace(".","_")+'").attr(\'onclick\',\'getChildren("'+oxid+'")\');';


                $('#getChildren'+oxid.replace(".","_")).attr('onclick',onclickHide);
                $('#getChildren'+oxid.replace(".","_")).attr('class','fa fa-caret-up fa-2x');

                $('[data-toggle="tooltip"]').tooltip()

                //console.log(resJSON);
                //console.log($('#tr'+oxid.replace(".","_")));
            },
            error: function(result){
                console.log('\nERROR');
            }
        });
    }

    function getArticle(oxid = null){

        if(oxid === null){
            oxid = $('input[name="oxid"]').val();
        }

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getArticle&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                oxid: oxid
            },
            success: function (result) {
                console.log(result);

                if(result.Status === 'real'){
                    $('#updateCreate')[0].innerHTML = 'Update';
                    $('#CardHeaderCreate')[0].innerHTML = 'Produktdaten updaten';
                    console.log($('#updateStatus'));
                    $('#updateStatus')[0].innerHTML = 'Uploadstatus: ' + result.Produktstatus;
                    $('#refreshArticle').show();
                    $('#realExisting').show();
                    $('#realNotExisting').hide();
                    $('#deleteProduct').show();
                    if(result.Produktstatus === "SUCCESS"){
                        $('#sellProduct').show();
                    } else {
                        $('#sellProduct').hide();
                    }

                    $("input[name='update']").val("1");

                } else if(result.Status === 'oxid'){
                    $('#updateCreate')[0].innerHTML = 'Anlegen';
                    $('#CardHeaderCreate')[0].innerHTML = 'Produktdaten anlegen';
                    $('#refreshArticle').hide();
                    $('#realExisting').hide();
                    $('#realNotExisting').show();
                    $('#deleteProduct').hide();
                    $('#sellProduct').hide();
                    $("input[name='update']").val("0");
                    ArtikelSucheReal(result.Titel);

                }

                $("input[name='oxid']").val(result.OXID);
                $("input[name='ean']").val(result.EAN);
                $("input[name='title']").val(result.Titel);
                $("input[name='list_price']").val(result.Preis);
                $("input[name='sellPrice']").val(result.OXPRICE);
                $("input[name='mpn']").val(result.Artikelnummer);
                $("input[name='picture']").val(result.Bild);
                $("input[name='category']").val(result.Kategorie);
                $("input[name='manufacturer']").val(result.Hersteller);
                $("textarea[name='short_description']").val(result.Kurzbeschreibung);
                $("textarea[name='short_description']").height( $("textarea[name='short_description']")[0].scrollHeight );
                $("textarea[name='description']").val(result.Beschreibung);
                //$("textarea[name='description']").height( $("textarea[name='description']")[0].scrollHeight );



                window.scrollTo(0,0);
            },
            error: function (result) {
                console.log(result + '\nERROR');
            }
        });
    }

    function completeCategory(val = null){
        var input = val;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getCategories&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                val: val
            },
            success: function(result){
                console.log(result);
                var select = '<select id="selectCategory" class="custom-select">';
                select += '<option selected>Wähle eine Kategorie</option>';
                for(var i = 0;i<result.length;i++){
                    select += '<option id="' + result[i].id_category + '" value="' + result[i].name + '">' + result[i].name + '</option>';
                }

                select += '</select>';

                var button = '<button type="button" class="btn btn-outline-success" onclick="saveCategoryVorschlag()">Übernehmen</button>';

                $('#testModal').modal();
                $('#testModalBody').empty();
                $('#testModalBody').append(select);
                $('#testModalFooter').empty();
                $('#testModalFooter').append(button);
            },
            error: function(result){
                console.log(result + '\nERROR');
            }
        });
    }

    function guessCategory(){


        var title = $("input[name='title']").val();
        var description = $("textarea[name='description']").val();
        var manufacturer = "";
        var picture = $("input[name='picture']").val();
        var price = $("input[name='list_price']").val();
        var keywords = [""];

        if(!title || !price || !description || !picture){
            alert('Bitte vervollständige zunächst Titel,Beschreibung,Preis und Bild');
            return;
        }



        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=guessCategory&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                title: title,
                description: description,
                price: price,
                picture: picture
            },
            success: function (result) {
                console.log(result);
                var select = '<select id="selectCategory" class="custom-select">';
                select += '<option selected>Wähle eine Kategorie</option>';
                for(var i = 0;i<result.length;i++){
                    select += '<option id="' + result[i].id_category + '" value="' + result[i].name + '">' + result[i].name + '</option>';
                }

                select += '</select>';

                var button = '<button type="button" class="btn btn-outline-success" onclick="saveCategoryVorschlag()">Übernehmen</button>';

                $('#testModal').modal();
                $('#testModalBody').append(select);
                $('#testModalFooter').append(button);

            },
            error: function (result) {
                console.log(result + '\nERROR');
            }
        });
    }

    function saveCategoryVorschlagAusListe(x,y){
        var arr = [];
        arr[0] = x;
        arr[1] = y;
        saveCategoryVorschlag(arr);
    }

    function saveCategoryVorschlag(suche = null){
        console.log(suche);
        var idcat = null;
        if(suche){
            var name = suche[0];
            idcat = suche[1];
            $("input[name='category']").val(name);

        } else {

            var auswahl = $('#selectCategory').val();
            $("input[name='category']").val(auswahl);

            idcat = $('#selectCategory')[0].selectedOptions[0].id;
        }

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getCategory&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                idcat: idcat
            },
            success: function(result){
                $('#pflichtAttribute').empty();
                $('#optAttribute').empty();

                console.log(result.mandatory_attributes);
                var reqAttr = result.mandatory_attributes;
                var optAttr = result.optional_attributes;
                var AttrArr = [];
                var name = "";
                var title = "";
                var input = "";
                var headline = "";
                var bool = true;
                for(var i = 0; i < reqAttr.length;i++){
                    name = reqAttr[i].name;
                    title = reqAttr[i].title;


                    if($("input[name='"+name+"'").length === 0 && $("textarea[name='"+name+"'").length === 0){
                        AttrArr.push(name);
                        if(bool){
                            headline = '<h3>weitere Pflichtattribute:</h3>';
                            $('#pflichtAttribute').append(headline);
                            //$(headline).insertBefore($('#updateCreate'));
                            bool = false;
                        }
                        input = '<div class="input-group">' +
                            '<div class="input-group-prepend">' +
                            '<span class="input-group-text" id="'+name+'">'+title+'</span>' +
                            '</div>' +
                            '<input required name="'+name+'" type="text" class="form-control" placeholder="'+title+'" aria-label="'+title+'">' +
                            '</div>';
                        $('#pflichtAttribute').append(input);
                        //$(input).insertBefore($('#updateCreate'));
                        console.log($("input[name='"+name+"'"));
                    }
                }
                bool = true;
                for(var j = 0; j < optAttr.length;j++){
                    name = optAttr[j].name;
                    title = optAttr[j].title;


                    if($("input[name='"+name+"'").length === 0 && $("textarea[name='"+name+"'").length === 0){
                        AttrArr.push(name);
                        if(bool){
                            headline = '<h3>Optionale Attribute:</h3>';
                            $('#optAttribute').append(headline);
                            //$(headline).insertBefore($('#updateCreate'));
                            bool = false;
                        }
                        input = '<div class="input-group">' +
                            '<div class="input-group-prepend">' +
                            '<span class="input-group-text" id="'+name+'">'+title+'</span>' +
                            '</div>' +
                            '<input name="'+name+'" type="text" class="form-control" placeholder="'+title+'" aria-label="'+title+'">' +
                            '</div>';

                        $('#optAttribute').append(input);

                        //$(input).insertBefore($('#updateCreate'));
                        console.log($("input[name='"+name+"'"));
                    }
                }
                console.log(AttrArr);
                $("input[name='additionalAttributes']").val(AttrArr);
            },
            error: function(result){
                console.log(result + '\nERROR');
            }
        });

        $('#testModalBody').empty();
        $('#testModal').modal("hide");

    }

    function deleteProduct(){
        if(window.confirm('Willst du wirklich dieses Produkt löschen?')){
            console.log('JA');
            var ean = $("input[name='ean']").val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: 'index.php?cl=[{$oView->getClassName()}]&fnc=deleteProductData&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                data: {
                    ean: ean
                },
                success: function(result){
                    console.log(result);
                    if(result === 204){
                        var oxid = $("input[name='oxid']").val();
                        getArticle(oxid);
                    } else {
                        alert('Da ist was schiefgelaufen!');
                    }
                },
                error: function(result){
                    console.log(result + '\nERROR');
                }

            });
        } else {
            console.log('NEIN');
        }
    }

    function sellProductAJAX(){
        var ean = $("input[name='ean']").val();
        var condition = $("#selectCondition").val();
        var price = $("input[name='sellPrice']").val();
        var bestand = $("input[name='sellBestand']").val();

        if(condition === 'Neu'){
            condition = 'new';
        } else return;

        $.ajax({
            type: 'POST',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=postUnit&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                ean: ean,
                condition: condition,
                price: price,
                bestand: bestand
            },
            success: function(result){
                if(result == ""){
                    alert('Artikel erfolgreich angelegt.');
                    $('#unitModal').modal('hide');
                }
            },
            error: function(result){
                console.log(result + '\nERROR');
            }

        });
    }

    /**
     * BEREICH ORDERS
     */
    function loadOrder(orderid,row){
        var thisRow = $(row);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getOrder&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            data: {
                orderid:orderid
            },
            success: function(result){
                thisRow.addClass('bg-success').siblings().removeClass('bg-success');
                console.log(thisRow);

                if($('#'+result.id_order+'importStatus')[0].innerHTML === 'Nein'){
                    $('#'+result.id_order+'importStatus')[0].innerHTML = 'Ja';
                }

                [{*TODO: Bei Live auskommentieren}]
                for(var i = 0;i< result.length;i++){
                    if(result[i].id_order === orderid){
                        loadOrderDetails(result[i]);
                    }
                }
                [{TODO: Bei Live auskommentieren*}]

                loadOrderDetails(result);
            },
            error: function(result){
                console.log(result + '\nERROR');
            }
        });
    }


    function loadOrderDetails(result) {


        $.ajax({
           type: 'POST',
           url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getImportStatus&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
           data: {
               orderid : result.id_order,
               order : result
           },
           success: function(res){
               console.log(res);
               $('#importOrder').show();
               if(result.seller_units[0].status === 'need_to_be_sent' && $('#'+result.id_order+'sendStatus')[0].innerHTML === 'Ja'){
                   $('#sendOrder').show();
               } else if(result.seller_units[0].status === 'sent' || result.seller_units[0].status === 'received'){
                   $('#orderSent').show();
               } else {
                   $('#sendOrder').hide();
                   $('#orderSent').hide();
               }
               /*if($('#'+result.id_order+'sendStatus')[0].innerHTML === 'Nein'){
               } else {
                   $('#sendOrder').hide();
               } */
           },
            error: function(result){
               console.log(result + "\nError");
            }
        });

        var bill= result.billing_address;
        var ship= result.shipping_address;
        $("input[name='orderemail']").val(result.buyer.email);
        $("input[name='buyerid']").val(result.buyer.id_buyer);
        $("input[name='orderid']").val(result.id_order);
        $("input[name='orderdate']").val(result.ts_created);
        //$("input[name='offerid']").val(result.id_offer);

        //$("input[name='deltimeexpires']").val(result.delivery_time_expires);

        $("input[name='status']").val(result.seller_units[0].status);




       // $("input[name='billNumber']").val(result.invoice.number);
       // $("input[name='billURL']").val(result.invoice.url);


        $("input[name='billVorname']").val(bill.first_name);
        $("input[name='billNachname']").val(bill.last_name);
        $("input[name='billFirma']").val(bill.company_name);
        $("input[name='billStrasse']").val(bill.street+ ' ' + bill.house_number);
        $("input[name='billOrt']").val(bill.postcode + ' ' +bill.city);
        $("input[name='billLand']").val(bill.country);
        $("input[name='billTelefon']").val(bill.phone);

        $("input[name='shipVorname']").val(ship.first_name);
        $("input[name='shipNachname']").val(ship.last_name);
        $("input[name='shipFirma']").val(ship.company_name);
        $("input[name='shipStrasse']").val(ship.street+ ' ' + ship.house_number);
        $("input[name='shipOrt']").val(ship.postcode + ' ' +ship.city);
        $("input[name='shipLand']").val(ship.country);
        $("input[name='shipTelefon']").val(ship.phone);

        /*for(var j = 0; j<item.length;j++){
            //neue Reihe in der Tabelle
        }*/
        var menge = 1;
        var price = 0;
        var shippingrate = 0;
        var sellerunits = result.seller_units;
        var modalContent = '';
        var orderunits = '';
        for(var i = 0; i<sellerunits.length;i++){
            price += sellerunits[i].price/100;
            shippingrate +=sellerunits[i].shipping_rate/100;
            modalContent += '<tr><td>'+sellerunits[i].item.id_item+'</td>'+
                            '<td>'+sellerunits[i].item.title+'</td>'+
                            '<td>'+menge+'</td>'+
                            '<td>'+sellerunits[i].price/100+' €</td>'+
                            '<td>'+sellerunits[i].price/100*menge+' €</td></tr>';
            orderunits += sellerunits[i].id_order_unit + ',';
        }
        orderunits = orderunits.substr(0,orderunits.length-1);
        $("input[name='orderunits']").val(orderunits);
        var tracking = $("input[name='"+result.id_order+"tracking']").val();
        $("input[name='tracking']").val(tracking);


        modalContent += '<tr><td>Porto</td>'+
                        '<td>Versandkosten</td>'+
                        '<td>1</td>'+
                        '<td>'+shippingrate+' €</td>'+
                        '<td>'+shippingrate+' €</td></tr>'
        modalContent += '<tr><td></td><td></td><td></td><td></td><td><b>'+(price+shippingrate)+' €</b></td></tr>';
        $('#billTable')[0].innerHTML = modalContent;

        $("input[name='price']").val((price+shippingrate) + ' €');


        /*var modalContent = '<tr>' +
                                '<td>'+result.item.id_item+'</td>'+
                                '<td>'+result.item.title+'</td>'+
                                '<td>'+menge+'</td>'+
                                '<td>'+result.price/100+'</td>'+
                                '<td>'+result.price/100*menge+'</td>'+
                           '</tr>'; */

        //$('#billTable')[0].innerHTML = modalContent;

    }

    function sendOrder(){
        var orderid = $("input[name='orderid']").val();
        var orderunits = $("input[name='orderunits']").val();
        var tracking = $("input[name='tracking']").val();

        $.ajax({
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sendOrder&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            type: 'POST',
            data: {
                orderid : orderid,
                orderunits : orderunits,
                tracking : tracking
            },
            success: function(result){
                console.log(result);
                location.reload();
            },
            error: function(e){
                console.log(e + "\nERROR");
            }
        })
    }

    function importToOxid(){

    }


    /**
     * BEREICH ANGEBOTE
     */


    /**
     * Diese Funktion sucht an Hand von EAN-Eingabe das zugehörige Angebot
     */

    function offerEanSearch(ean){

        console.log(ean);
        $.ajax({
           type: 'GET',
           dataType: 'json',
           url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getUnitByEan&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
           data: {
               ean: ean
           },
            success: function(result){
               console.log(result);
               var offer = result;
               $('#tableOffers').empty();
               if(offer[0]){
                   var idunit = offer[0].item.title;
                   var newtr = '<tr style="cursor:pointer" onclick="showDetails(\''+offer[0].id_unit+'\',\''+offer[0].item.title+'\',\''+offer[0].amount+'\',\''+offer[0].price/100+'\',\''+offer[0].delivery_time_min+'\',\''+offer[0].delivery_time_max+'\',\''+offer[0].shipping_group+'\',\''+offer[0].shipping_rate/100+'\')">\n' +
                                   '<td>'+offer[0].id_unit+'</td>' +
                                   '<td>'+offer[0].condition+'</td>' +
                                   '<td>'+offer[0].amount+'</td>' +
                                   '<td>'+offer[0].price/100+' €</td>' +
                                   '<td>'+offer[0].item.title+'</td>' +
                                   '<td>'+offer[0].item.eans[0]+'</td>' +
                                   '<td>'+offer[0].item.is_valid+'</td>' +
                                   '<td>'+offer[0].date_inserted+'</td>' +
                                   '<td>'+offer[0].shipping_group+'</td>' +
                                '</tr>';
                   $('#tableOffers').append(newtr);
               }
            },
            error: function(e){
               console.log('ERROR: \n' + e)
            }
        });
    }

    function showDetails(unit,title,amount,price,deltimemin,deltimemax,shipping_group,shipping_rate){
        $('input[name="id_unit"]').val(unit);
        $('input[name="title"]').val(title);
        $('input[name="amount"]').val(amount);
        $('input[name="price"]').val(price);
        $('input[name="delivery_time_min"]').val(deltimemin);
        $('input[name="delivery_time_max"]').val(deltimemax);
        $('input[name="shipping_group"]').val(shipping_group);
        $('input[name="shipping_rate"]').val(shipping_rate);
    }

    function updateUnit(){
        var unit = $('input[name="id_unit"]').val();
        var amount = $('input[name="amount"]').val();
        var mintime = $('input[name="delivery_time_min"]').val();
        var maxtime = $('input[name="delivery_time_max"]').val();
        var price = $('input[name="price"]').val() *100;
        $.ajax({
           type: 'POST',
           url: 'index.php?cl=[{$oView->getClassName()}]&fnc=updateUnit&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
           data: {
               unit : unit,
               amount : amount,
               price : price,
               mintime : mintime,
               maxtime : maxtime
           },
            success: function(result){
                console.log(result);
                location.reload();
            },
            error: function(result){
               console.log(result + '\nERROR');
                location.reload();
            }
        });
    }

    /**
     * BEREICH RETOUREN
     */

    /**
     *
     * @param id = Return-ID
     */
    function getReturn(id){
        $.ajax({
            type: 'POST',
            url: 'index.php?cl=[{$oView->getClassName()}]&fnc=getReturn&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
            dataType: 'html',
            data: {
                id : id
            },
            success: function(result){
                alert(result);
            },
            error: function(result){
                console.log(result + '\nERROR');
            }
        });
    }

</script>
    [{/if}]