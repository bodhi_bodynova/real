<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 2019-03-07
 * Time: 13:58
 */

$sLangName = "Deutsch";
$iLangNr = 0;
$aLang = array(
    'charset'                                  => 'UTF-8 ',
    'REAL_MAINMENU'                        => 'Real.de',
    'REAL_SUBMENU_ARTICLES'                => 'Artikel',
    'REAL_SUBMENU_ORDERS'                    => 'Bestellungen',
    'REAL_SUBMENU_OFFERS'                    => 'Angebote',
    'REAL_SUBMENU_INVOICES'         => 'Rechnungen',
    'REAL_SUBMENU_INVENTORY'                 => 'Lager',
    'REAL_SUBMENU_TICKETS'              => 'Tickets',
    'REAL_SUBMENU_RETURNS'               => 'Retouren',
    'REAL_SUBMENU_REPORTS'              => 'Reports',
    'GENERAL_OXID'                             => 'OXID',
    'GENERAL_OXPARENTID'                       => 'OXPARENTID'
);