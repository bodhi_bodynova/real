<?php


namespace ewald\real\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;


class real_orders extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController

{
    protected $_sClass = 'real_orders';

    protected $_sThisTemplate = 'real_orders.tpl';

    protected $_aViewData = null;

    protected $_sTestJSON = null;

    public $_sOrdernr = null;

    public function __construct($auto = null){
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

        //$path = '/order-units/seller/?status=open&sort=ts_created:desc&limit=30';
        if($auto){
            $path = '/orders/seller/?limit=10';
        } else {
            $path = '/orders/seller/?limit=75';
        }
        $api = new real_api();
        $arrOrders = array();
        $orders = $api->getRequest($path);
        $this->_aViewData['orders'] = $orders;

        if($auto){
            return;
        }
        $i = 0;
        foreach($this->_aViewData['orders'] as $key){
            $orderNr = $key['id_order'];
            $result = $oDb->getAll('SELECT * FROM realorder WHERE realorderid = ?',array($orderNr));
            if($result){
                $this->_aViewData['orders'][$i]['import'] = 'Ja';
                $oxorderid = $result[0]['oxorderid'];
                $trackingcode = $oDb->getOne('SELECT OXTRACKCODE FROM oxorder WHERE OXID = ?', array($oxorderid));
                if($trackingcode){
                    $this->_aViewData['orders'][$i]['tracking'] = $trackingcode;
                    $this->_aViewData['orders'][$i]['versandbereit'] = 'Ja';
                    if($result[0]['status'] == 'sent'){
                        $this->_aViewData['orders'][$i]['versendet'] = 'Ja';
                    } else {
                        $this->_aViewData['orders'][$i]['versendet'] = 'Nein';
                    }
                } else {
                    $this->_aViewData['orders'][$i]['versandbereit'] = 'Nein';
                }
            } else {
                // Hier Stelle für automatisches Importieren!!!
                $this->_aViewData['orders'][$i]['import'] = 'Nein';

            }
            $i++;
        }
        return null;
    }

    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');
    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }


    public function getOrder($orderid = null){
        //die($this->_sTestJSON);
        $auto = true;
        if(!$orderid){
            $orderid = Registry::getConfig()->getRequestParameter('orderid');
            $auto = false;
        }
        $path = '/orders/' . $orderid . '/?embedded=billing_address,buyer,seller_units,shipping_address,order_invoices';
        $api = new real_api();
        $result = $api->getRequest($path);

        if($auto){
            return $result;
        } else {
            print_r(json_encode($result));
            die();
        }

        print_r(json_encode($result));
        die();
        return $result;

        return json_decode($this->_sTestJSON);
    }

    public function sendOrder(){
        // TODO: Deprecated checken
        //$orderunits = (new \OxidEsales\Eshop\Core\Request)->getRequestEscapedParameter('orderunits');
        $orderunits = Registry::getConfig()->getRequestParameter('orderunits');
        $tracking = Registry::getConfig()->getRequestParameter('tracking');
        $orderid = Registry::getConfig()->getRequestParameter('orderid');

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $query = 'UPDATE realorder SET status = "sent" WHERE realorderid = ?';
        $oDb->execute($query,array($orderid));

        $arrOrderunits = explode(',',$orderunits);
        foreach($arrOrderunits as $key){
            $path = '/order-units/' . $key . '/send/';
            $data = [
                "carrier_code" => "DHL",
                "tracking_number" => $tracking
            ];
            $api = new real_api();
            $result = $api->patchRequest($path,$data);

            // TODO: Fehler abfangen!

            print_r($result);
        }
        die();
    }


    public function getImportStatus($order = null){
        $auto = true;

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $orderid = Registry::getConfig()->getRequestParameter('orderid');
        if(!$order){
            $order = Registry::getConfig()->getRequestParameter('order');
            $auto = false;
        }

        //print_r($order);
        /**
         * TODO: Werte der Order in Variablen stecken. Anschließend Tabellen befüllen:
         * TODO: oxuser, oxorder -> oxorderarticles, realorder
         */

        $exist = 0;
        // OXUSER
        $oxusername = $order['buyer']['email'];
        $resultOxuser = $oDb->getAll('SELECT * FROM oxuser WHERE OXUSERNAME = ?',array($oxusername));
        if($resultOxuser){
            $exist = 1;
            $oxidoxuser = $resultOxuser[0]['OXID'];
        } else {
            $oxidoxuser = md5(uniqid());
        }

        $oxrights = 'user';
        $oxfname = $order['shipping_address']['first_name'];
        $oxlname = $order['shipping_address']['last_name'];
        $oxstreet = $order['shipping_address']['street'];
        $oxstreetnr = $order['shipping_address']['house_number'];
        $oxcity = $order['shipping_address']['city'];
        $oxzip = $order['shipping_address']['postcode'];
        $oxsal = $order['shipping_address']['gender'];
        $oxcreate = date('Y-m-d H:i:s');

        if(!$exist){
            $insertUser = 'INSERT INTO oxuser (OXID,OXRIGHTS,OXUSERNAME,OXFNAME,OXLNAME,OXSTREET,OXSTREETNR,OXCITY,OXZIP,OXSAL,OXCREATE) VALUES(?,?,?,?,?,?,?,?,?,?,?)';
            $oDb->execute($insertUser,array(
                $oxidoxuser,
                $oxrights,
                $oxusername,
                $oxfname,
                $oxlname,
                $oxstreet,
                $oxstreetnr,
                $oxcity,
                $oxzip,
                $oxsal,
                $oxcreate
            ));
        }

        // REALORDER TODO: GIBT ES DIESE REALORDER SCHON? Wenn nicht, importiere!
        $oxidrealorder = md5(uniqid());
        // OXORDER

        $oxidoxorder = md5(uniqid());

        $realorderid = $order['id_order'];
        $buyerid = $order['buyer']['id_buyer'];
        $buyeremail = $oxusername;
        $offerid = "";//$order['id_offer'];
        $lieferfrist = "";//$order['delivery_time_expires'];
        $status = "";//$order['status'];
        $oxuserid = $oxidoxuser;




        $oxorderdate = $order['ts_created'];
        $oxbillcompany = $order['billing_address']['company_name'];
        $oxbillemail = $oxusername;
        $oxbillfname = $order['billing_address']['first_name'];
        $oxbilllname = $order['billing_address']['last_name'];
        $oxbillgender = $order['billing_address']['gender'];
        $oxbillstreet = $order['billing_address']['street'];
        $oxbillstreetnr = $order['billing_address']['house_number'];
        $oxbillcity = $order['billing_address']['city'];
        $oxbillzip = $order['billing_address']['postcode'];
        $oxbillfon = $order['billing_address']['phone'];
        $oxbillcountry = $order['billing_address']['country'];

        $oxdelcompany = $order['shipping_address']['company_name'];
        $oxdelfname = $oxfname;
        $oxdellname = $oxlname;
        $oxdelgender = $order['shipping_address']['gender'];
        $oxdelstreet = $oxstreet;
        $oxdelstreetnr = $oxstreetnr;
        $oxdelcity = $oxcity;
        $oxdelzip = $oxzip;
        $oxdelfon = $order['shipping_address']['phone'];
        $oxdelcountry = $order['shipping_address']['country'];


        $oxtotalnetsum = 0;
        $oxtotalbrutsum = 0;
        $oxtotalordersum = 0;
        $oxartvat1 = 19;
        $oxartvatprice1 = 0;
        $oxdelcost = 0;
        $oxdelvat = 19;
        foreach($order['seller_units'] as $key){
            $oxdelcost += $key['shipping_rate']/100;
            $oxtotalbrutsum += $key['price']/100;
        }
        $oxtotalordersum = $oxdelcost+$oxtotalbrutsum;
        $oxtotalnetsum = round($oxtotalbrutsum / 1.19,2);
        $oxartvatprice1 = $oxtotalbrutsum-$oxtotalnetsum;
        /*
        $oxtotalnetsum = $order['revenue_net']/100;
        $oxtotalbrutsum = $order['revenue_gross']/100;
        $oxtotalordersum = $order['price']/100;
        $oxartvat1 = 19;
        $oxartvatprice1 = $oxtotalbrutsum-$oxtotalnetsum;
        $oxdelcost = $oxtotalordersum-$oxtotalbrutsum;
        $oxdelvat = 19;
        */

        // REALORDER TODO: GIBT ES DIESE REALORDER SCHON? Wenn nicht, importiere!

        $realorderProof = 'SELECT * FROM realorder WHERE realorderid = ?';
        $resultRealorder = $oDb->getAll($realorderProof,array($realorderid));

        if(!$resultRealorder){

            $oxordernr = $oDb->getOne('SELECT OXCOUNT FROM oxcounters');
            //$oxordernr = oxNew(Oxid\Core\Counter::class)->getNext($this->_getCounterIdent());
            $billLandId = $oDb->getOne('SELECT OXID FROM oxcountry WHERE OXISOALPHA2 = ?',array($oxbillcountry));
            $delLandId = $oDb->getOne('SELECT OXID FROM oxcountry WHERE OXISOALPHA2 = ?', array($oxdelcountry));
            $oxordernr++;

            $oDb->execute('UPDATE oxcounters SET OXCOUNT = ?',array($oxordernr));
            $insertRealorder = 'INSERT INTO realorder (OXID,oxorderid,realorderid,buyerid,buyeremail,ordercreated,offerid,lieferfrist,status,shipvorname,shipnachname,shipgender,shipfirma,shipstrasse,shiphausnummer,shipplz,shipstadt,shiptelefon,shipland,billvorname,billnachname,billgender,billfirma,billstrasse,billhausnummer,billplz,billstadt,billtelefon,billland) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            $oDb->execute($insertRealorder,array(
                $oxidrealorder,
                $oxidoxorder,
                $realorderid,
                $buyerid,
                $buyeremail,
                $oxorderdate,
                $offerid,
                $lieferfrist,
                $status,
                $oxdelfname,
                $oxdellname,
                $oxdelgender,
                $oxdelcompany,
                $oxdelstreet,
                $oxdelstreetnr,
                $oxdelzip,
                $oxdelcity,
                $oxdelfon,
                $delLandId,
                $oxbillfname,
                $oxbilllname,
                $oxbillgender,
                $oxbillcompany,
                $oxbillstreet,
                $oxbillstreetnr,
                $oxbillzip,
                $oxbillcity,
                $oxbillfon,
                $billLandId

            ));

            $oxorderdateNOW = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")));


            $insertOxOrder = 'INSERT INTO oxorder (OXID,OXUSERID,OXORDERDATE,OXORDERNR,OXBILLCOMPANY,OXBILLEMAIL,OXBILLFNAME,OXBILLLNAME,OXBILLSTREET,OXBILLSTREETNR,OXBILLCITY,OXBILLCOUNTRYID,OXBILLZIP,OXBILLFON,OXDELCOMPANY,OXDELFNAME,OXDELLNAME,OXDELSTREET,OXDELSTREETNR,OXDELCITY,OXDELCOUNTRYID,OXDELZIP,OXDELFON,OXTOTALNETSUM,OXTOTALBRUTSUM,OXTOTALORDERSUM,OXARTVAT1,OXARTVATPRICE1,OXDELCOST,OXDELVAT,OXPAYMENTTYPE,oxshop,OXREMARK,OXFOLDER,OXSHOPID) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

            $oDb->execute($insertOxOrder,array(
                $oxidoxorder,
                $oxuserid,
                $oxorderdateNOW,
                $oxordernr,
                $oxbillcompany,
                $oxbillemail,
                $oxbillfname,
                $oxbilllname,
                $oxbillstreet,
                $oxbillstreetnr,
                $oxbillcity,
                $billLandId,
                $oxbillzip,
                $oxbillfon,
                $oxdelcompany,
                $oxdelfname,
                $oxdellname,
                $oxdelstreet,
                $oxdelstreetnr,
                $oxdelcity,
                $delLandId,
                $oxdelzip,
                $oxdelfon,
                $oxtotalnetsum,
                $oxtotalbrutsum,
                $oxtotalordersum,
                $oxartvat1,
                $oxartvatprice1,
                $oxdelcost,
                $oxdelvat,
                'real',
                'real',
                $realorderid,
                'ORDERFOLDER_NEW',
                2
            ));


            foreach($order['seller_units'] as $key){
                /*$ean = $key['item']['eans'][0];
                $api = new real_api();
                $path = '/product-data/' . $ean . '/';
                $result = $api->getRequest($path);
                $oxartnum = $result['mpn'][0];*/




                if($oxartnum && $oxartnum == $key['id_offer']){

                } else {
                    $oxartnum = $key['id_offer'];
                    $queryArticles = 'SELECT * FROM oxarticles WHERE OXARTNUM = ?';
                    try {
                        $articles = $oDb->getAll($queryArticles, array($oxartnum));
                    } catch (DatabaseErrorException $e) {
                        echo 'ERROR: ' . $e->getMessage() . '<br> BITTE ORDER ' . $oxidoxorder . ' prüfen!!';
                        die();
                    }

                    $oxartid = $articles[0]['OXID'];

                    $oArticle = new Article();
                    $oArticle->disableLazyLoading();
    
                    $oArticle->load($oxartid);

                    if(!empty($oArticle->oxarticles__oxparentid->value)){
                        $oParent = new Article();
                        $oParent->load($oArticle->oxarticles__oxparentid->value);
                        $oxtitle = $oParent->oxarticles__oxtitle->value;//$articles[0]['OXTITLE'];
                        $oxshortdesc = $oParent->oxarticles__oxshortdesc->value;//$articles[0]['OXSHORTDESC'];
                    }else{
                        $oxtitle = $oArticle->oxarticles__oxtitle->value;//$articles[0]['OXTITLE'];
                        $oxshortdesc = $oArticle->oxarticles__oxshortdesc->value;//$articles[0]['OXSHORTDESC'];
                    }
                }



                // OXORDERARTICLES TODO: Warte auf erste Bestellung


                $oxidoxorderarticles = md5(uniqid());
                $oxamount = 1;

                //$oxartnum = $articles[0]['OXARTNUM'];

                $oxselvariant = $oArticle->oxarticles__oxvarselect->value;
                $oxbrutprice = $key['price']/100;
                $oxvat = 19;
                $oxnetprice = round($oxbrutprice/1.19,2);
                $oxvatprice = $oxbrutprice-$oxnetprice;
                $oxprice = $oArticle->oxarticles__oxprice->value;//$articles[0]['OXPRICE'];
                $oxbprice = $oxbrutprice;
                $oxnprice = $oxnetprice;

                $queryInsertOrderArticles = 'INSERT INTO oxorderarticles (OXID,OXORDERID,OXAMOUNT,OXARTID,OXARTNUM,OXTITLE,OXSHORTDESC,OXSELVARIANT,OXNETPRICE,OXBRUTPRICE,OXVATPRICE,OXVAT,OXPRICE,OXBPRICE,OXNPRICE) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

                $oDb->execute($queryInsertOrderArticles,array(
                    $oxidoxorderarticles,
                    $oxidoxorder,
                    $oxamount,
                    $oxartid,
                    $oxartnum,
                    $oxtitle,
                    $oxshortdesc,
                    $oxselvariant,
                    $oxnetprice,
                    $oxbrutprice,
                    $oxvatprice,
                    $oxvat,
                    $oxprice,
                    $oxbprice,
                    $oxnprice
                ));
            }
        } else {
            if($auto){
                return;
            } else {
                die("0");
            }
        }
        if(!$auto){
            die();
        }
    }

    public function getOrderNr(){
        $arrResult = array();
        foreach($this->_aViewData['orders'] as $key) {
            array_push($arrResult,$key['id_order']);
        }
        return $arrResult;
    }

}