<?php
namespace ewald\real\Application\Controller\Admin;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Request;

class real_articles extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController

{
    protected $_sClass = 'real_articles';

    protected $_sThisTemplate = 'real_articles.tpl';

    protected $_aViewData = null;

    protected $_sOxid = null;

    protected $_oDb = null;

    protected $_sSecretKey = null;

    protected $_oApi = null;

    protected $_sEan = null;

    /**
     * real_articles constructor. TODO: Hier alle Variablen beschreiben, die man auf jeden Fall benötigt, auch mehrfach?
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */

    public function __construct()
    {
        $this->_oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $this->_sSecretKey = Registry::getConfig()->getConfigParam('sRealSecretKey');
        $this->_sClientKey = Registry::getConfig()->getConfigParam('sRealClientKey');
        $this->_oApi = new real_api();
    }

    public function getToken()
    {
        $req = new Request();
        return $req->getRequestEscapedParameter('stoken');
        //return Registry::getConfig()->getRequestParameter('stoken');
    }

    public function getAdminSid()
    {
        $req = new Request();
        return $req->getRequestEscapedParameter('force_admin_sid');
        //return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }


    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }

    /**
     * Simuliert die Suche im Real Onlineshop. Schlägt schon direkt am Anfang Kategorien vor
     */
    public function realSuche(){
        $req = new Request();
        $suchparam = $req->getRequestEscapedParameter('suchparam');
        $path = '/items/?q=' . urlencode(trim($suchparam)) . '&embedded=category';
        $api = new real_api();
        $result = $api->getRequest($path);
        print_r(json_encode($result));
        die();
    }




    /**
     * Bereitet alle relevanten Daten des ausgewählten Artikel für den Upload zu Real vor.
     * TODO: Falls der Artikel schon vorhanden ist, gebe Hinweis, dass nur ein Update erfolgen kann.
     * @param null $oxid
     * @return object|Article
     */
    public function getArticle(){
        $req = new Request();
        $oxid = $req->getRequestEscapedParameter('oxid');
        //$oxid = Registry::getConfig()->getRequestRawParameter('oxid');
        $oArticle = new Article();
        $oArticle->setLoadParentData(true);
        $oArticle->load($oxid);



        //TODO: CHECK
        $ean = $oArticle->oxarticles__oxean->value;
        $arrReturn['OXPRICE'] = $oArticle->oxarticles__oxprice->value;
        $this->_sEan = $ean;
        if($ean){
            //$res = $this->getStatus($ean);
            $res = $this->_getProductData();

            if(!$res['message']){

                $arrReturn['OXID'] = $oxid;
                $arrReturn['EAN'] = $res['ean'][0];
                $arrReturn['Beschreibung'] = $res['description'][0];
                $arrReturn['Kurzbeschreibung'] = $res['short_description'][0];
                $arrReturn['Preis'] = $res['list_price'][0];
                $arrReturn['Artikelnummer'] = $res['mpn'][0];


                $Bilder = $res['picture'][0];
                $i = 1;
                for($i=1;$i<sizeof($res['picture']);$i++){
                    $Bilder .= ',' . $res['picture'][$i];
                }
                $arrReturn['Bild'] =  $Bilder;


                $arrReturn['Kategorie'] =  $res['category'][0];
                $arrReturn['Hersteller'] =  $res['manufacturer'][0];

                $arrReturn['Titel'] = $res['title'][0];
                $arrReturn['Status'] = 'real';

                $productStatus = $this->getStatus($ean);
                $arrReturn['Produktstatus'] = $productStatus['update_status'];
                $arrReturn['id_item'] = $productStatus['id_item'];



                // TODO: Status Item überprüfen! Falls noch kein Angebot -> Button erstellen für Angebot..
                $idItem = $productStatus['id_item'];



                echo json_encode($arrReturn);
                die();
            }
        }
        $sLongDescription = $oArticle->getLongDesc();


        $arrReturn['OXID'] = $oArticle->oxarticles__oxid->value;
        $arrReturn['EAN'] = $ean;
        $arrReturn['Beschreibung'] = $sLongDescription;
        $arrReturn['Kurzbeschreibung'] = $oArticle->oxarticles__oxshortdesc->value;
        $arrReturn['Preis'] = $oArticle->oxarticles__oxprice->value;
        $arrReturn['Artikelnummer'] = $oArticle->oxarticles__oxartnum->value;

        $Bilder = 'https://cdn.bodynova.de/out/pictures/master/product/1/' . $oArticle->oxarticles__oxpic1->value;
        $i = 2;
        $sBild = 'oxarticles__oxpic2';
        while($oArticle->$sBild->value){
            $Bilder .= ',https://cdn.bodynova.de/out/pictures/master/product/' . $i . '/' . $oArticle->$sBild->value;
            $i++;
            $sBild = 'oxarticles__oxpic' . $i;
            if($i>8){
                break;
            }
        }

        $arrReturn['Bild'] =  $Bilder;
        //$arrReturn['Bild'] =  'https://cdn.bodynova.de/out/pictures/master/product/1/' . $oArticle->oxarticles__oxpic1->value;
        $arrReturn['Hersteller'] = $sManufacturer = $this->getManufacturer($oArticle->oxarticles__oxmanufacturerid->value);
        $arrReturn['Titel'] = $oArticle->oxarticles__oxtitle->value . ' ' . $oArticle->oxarticles__oxvarselect->value;
        $arrReturn['Status'] = 'oxid';

        echo json_encode($arrReturn);
        die();
        //TODO: CHECK


    }

    /**
     * Suchfunktion zur Artikelsuche im Backend (Benutzt SOLR, lokal kann es zu Unterschieden kommen)
     */
    public function getArticles(){
        $req = new Request();
        $search = $req->getRequestEscapedParameter('suche');
        //$search = Registry::getConfig()->getRequestRawParameter('suche');

        $curl_session = curl_init();
        curl_setopt($curl_session ,CURLOPT_URL,"https://sales.bodynova.com:8983/solr/bodynova/select?q=" . $search);
        curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($curl_session);
        curl_close($curl_session);
        $this->_aViewData['curl'] = json_decode($result);
        return;
    }

    /**
     * Liefert die Varianten zurück, damit sie im Backend dargestellt werden können
     * @return array Alle Varianten des dargestellten Vaters
     */
    public function getChildren(){
        $req = new Request();
        $oxid = $req->getRequestEscapedParameter('oxid');
        //$oxid = Registry::getConfig()->getRequestRawParameter('oxid');
        $result = null;
        try{
            $query = "SELECT OXID,OXARTNUM,OXTITLE,OXEAN FROM oxarticles WHERE OXPARENTID = ?";
            $result = $this->_oDb->getAll($query,array($oxid));

            $i = 0;
            foreach($result as $key){
                $result[$i][4] = $this->getStatus($key[3]);
                $i++;
            }

            echo json_encode($result);
            die();
        } catch(\Exception $e){
            echo 'Exception abgefangen: ', $e->getMessage(), "\n";
        }
        return $result;
    }

    /**
     * Diese Funktion legt einen Artikel bei Real an oder aktualisiert die Daten eines bereits bestehenden Artikels.
     */
    public function createArticle(){
        $req = new Request();
        $id = $req->getRequestEscapedParameter('id');


        $ean = $req->getRequestEscapedParameter('ean');
        $title = $req->getRequestEscapedParameter('title');
        $category = $req->getRequestEscapedParameter('category');
        $description = $req->getRequestEscapedParameter('description');
        $short_description = $req->getRequestEscapedParameter('short_description');
        $mpn = $req->getRequestEscapedParameter('mpn');
        $list_price = strval($req->getRequestEscapedParameter('list_price'));
        $picture = $req->getRequestEscapedParameter('picture');

        $picture = explode(',',$picture);
        $manufacturer = $req->getRequestEscapedParameter('manufacturer');

        $additionalAttributes = $req->getRequestEscapedParameter('additionalAttributes');
        $additionalAttributesArray = explode(',',$additionalAttributes);
        $arrVal = $req->getRequestEscapedParameter('arrVal');


        $arrAddAlternative = array();
        array_push($arrAddAlternative,array(
            "attribute" => "manufacturer",
            "value" => array($manufacturer)
        ));

        $i=0;
        foreach($additionalAttributesArray as $key){
            array_push($arrAddAlternative,array(
                "attribute" => $key,
                "value" => array($arrVal[$i])
            ));
            $i++;
        }
       /* print_r($arrAddAlternative);
        die();

        // weitere Pflichtattribute... TODO: Automatisch einsammeln
        $material_composition = Registry::getConfig()->getRequestRawParameter('material_composition');
        // weitere Pflichtattribute... TODO: Automatisch einsammeln



        $arrAdd = array(
            array(
                "attribute" => "manufacturer",
                "value" => array($manufacturer)
            ),
            array(
                "attribute" => "material_composition",
                "value" => array($material_composition)
            )
        );
        print_r($arrAdd);
        die();*/
        $data = [
            "ean"       => array($ean),
            "title"     => array($title),
            "category"  => array($category),
            "description"       => array($description),
            "short_description" => array($short_description),
            "mpn" => array($mpn),
            //"price" => array($list_price),
            "picture" => $picture,
            "additional_attributes" => $arrAddAlternative
        ];


        $api = new real_api();
        $path = '/product-data/' . $ean . '/';


        if($req->getRequestEscapedParameter('update') == "1"){
            $api->patchRequest($path,$data);
        } else {
            $api->putRequest($path,$data);
        }

        //print_r($this->getStatus($ean));
        //die();


        /*
        $jsonData = json_encode($data);

        $uri = 'https://www.real.de/api/v1/product-data/' . $ean . '/';


        $timestamp = time();

        $headers = [
            'Accept: application/json',
            'Hm-Client: ' . $clientKey,
            'Hm-Timestamp: ' . $timestamp,
            'Hm-Signature: ' . $api->signRequest('PUT', $uri, $jsonData, $timestamp, $secretKey),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, 1);

        $response = curl_exec($ch);

        list($header, $body) = explode("\r\n\r\n", $response, 2);

        print("$header\n\n");

        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        print("HTTP Response Code: $responseCode\n");

        curl_close($ch);

        $this->getStatus($ean);
        */
        //die();

    }

    public function deleteProductData(){
        $req = new Request();
        $ean = $this->_sEan;
        if(!$ean){
            $ean = $req->getRequestEscapedParameter('ean');
        }
        $path = '/product-data/' . $ean . '/';
        $api = new real_api();
        $res = $api->deleteRequest($path);
        echo json_encode($res);
        die();
    }

    public function _getProductData(){
        $ean = $this->_sEan;
        $path = '/product-data/';
        $api = new real_api();
        return $api->getRequest($path,$ean);
    }

    /**
     * Hier wird an Hand der EAN der aktuelle Status des Artikels abgefragt, passiert in der Regel direkt nach Produkterstellung
     * Liefert ein Ergebnis an den Produktverwalter im Backend zurück.
     * @param $ean
     */
    public function getStatus($ean){
        $path = '/product-data-status/';
        $api = new real_api();
        $item = $api->getRequest($path,$ean);
        return $item;
    }


    /**
     * Hier wird ein Vorschlag für eine Produktkategorie erfragt und zurückgegeben
     */
    public function guessCategory(){
        $req = new Request();
        $path = '/categories/decide/';

        $data = [
            "item"           => array(
                "title" => $req->getRequestEscapedParameter('title'),
                "description" => $req->getRequestEscapedParameter('description'),
                "manufacturer" => "",
                "pictures" => array(
                    $req->getRequestEscapedParameter('picture')
                ),
            ),
            "price" => intval($req->getRequestEscapedParameter('price')),
            "keywords" => array(
                ""
            )
        ];

        $api = new real_api();

        $result = $api->postRequest($path,$data);
        print_r($result);
        die();
    }

    /**
     * Gibt eine Auswahl an Kategorien zurück, die zur eingegebenen Phrase passen.
     */
    public function getCategories(){
        $path = '/categories/?q=' . urlencode(trim($req->getRequestEscapedParameter('val'))) . '&limit=25';

        $api = new real_api();
        $result = $api->getRequest($path);
        print_r(json_encode($result));
        die();
    }

    /**
     * Schlägt anhand von Produktinformationen 5 Kategorien vor, die passend für das Produkt sein könnten..
     */
    public function getCategory(){
        $path = '/categories/' . Registry::getConfig()->getRequestParameter('idcat') . '/?embedded=optional_attributes,mandatory_attributes';

        $api = new real_api();
        $result = $api->getRequest($path);
        print_r(json_encode($result));
        die();
    }


    public function getManufacturer($oxid = null){
        if($oxid){
            $query = 'SELECT OXTITLE FROM oxmanufacturers WHERE OXID = ?';
            $result = $this->_oDb->getOne($query,array($oxid));
            return $result;
        } else return "";

    }


    /**
     * TODO: Umziehen nach Offers!?!?
     */
    public function postUnit(){
        $path = '/units/';

        $ean = Registry::getConfig()->getRequestParameter('ean');
        $condition = Registry::getConfig()->getRequestParameter('condition');
        $price = Registry::getConfig()->getRequestParameter('price');
        $bestand = Registry::getConfig()->getRequestParameter('bestand');


        $data = [
            "ean"           => $ean,
            "condition"     => $condition,
            "listing_price" => intval($price*100),
            "amount"        => intval($bestand),
            "delivery_time_min" => 1,
            "delivery_time_max" => 2
        ];

        $api = new real_api();

        $result = $api->postRequest($path,$data);
        print_r($result);
        die();
    }


}