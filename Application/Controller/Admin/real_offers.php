<?php
namespace ewald\real\Application\Controller\Admin;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Request;

class real_offers extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController
{

    protected $_sClass = 'real_offers';

    protected $_sThisTemplate = 'real_offers.tpl';

    protected $_aViewData = null;

    protected $_oDb = null;

    protected $_sSecretKey = null;

    protected $_sClientKey = null;

    protected $_oApi = null;

    public function __construct()
    {
        $this->_oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $this->_sSecretKey = Registry::getConfig()->getConfigParam('sRealSecretKey');
        $this->_sClientKey = Registry::getConfig()->getConfigParam('sRealClientKey');
        $this->_oApi = new real_api();
        //$this->_getInitialUnits();
    }


    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }

    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');
    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    /**
     * TODO: 1. Hole alle Units ab... (Vorschau mit den relevanten Daten)
     */
    public function getOffers(){
        $this->_aViewData['units'] = new \stdClass;
    }

    /**
     * TODO: Bei Klick auf eine Unit von oben -> lade ALLE Daten.. Mit Möglichkeit, das Angebot zu bearbeiten..
     */

    protected function _getInitialUnits(){
        $path = '/units/seller/?limit=10&embedded=item';
        $result = $this->_oApi->getRequest($path);
        $i = 0;

        foreach($result as $key){
            $this->_aViewData['units'][$i] = new \stdClass();
            $this->_aViewData['units'][$i]->id_unit =    $key['id_unit'];

            $this->_aViewData['units'][$i]->id_item =    $key['id_item'];
            $this->_aViewData['units'][$i]->condition =  $key['condition'];
            $this->_aViewData['units'][$i]->amount =     $key['amount'];
            $this->_aViewData['units'][$i]->deltimemin = $key['delivery_time_min'];
            $this->_aViewData['units'][$i]->deltimemax = $key['delivery_time_max'];
            $this->_aViewData['units'][$i]->price =      $key['price']/100;
            $this->_aViewData['units'][$i]->itemtitle =  $key['item']['title'];
            $this->_aViewData['units'][$i]->itemean =    $key['item']['eans'][0];
            $this->_aViewData['units'][$i]->itemurl =    $key['item']['url'];
            $this->_aViewData['units'][$i]->itemvalid =  $key['item']['is_valid'] ? 'Ja' : 'Nein';
            $this->_aViewData['units'][$i]->iteminsert = $key['date_inserted'];
            $this->_aViewData['units'][$i]->shippinggroup = $key['shipping_group'];
            $this->_aViewData['units'][$i]->shippingrate = $key['shipping_rate']/100;
            $i++;
        }
    }

    public function getUnitByEan(){
        $req = new Request();
        $ean = $req->getRequestEscapedParameter('ean');
        $path = '/units/seller/?ean=' . $ean . '&embedded=item';
        $result = $this->_oApi->getRequest($path);
        print_r(json_encode($result));
        die();
    }

    public function updateUnit(){
        $unit = Registry::getConfig()->getRequestParameter('unit');
        $amount = Registry::getConfig()->getRequestParameter('amount');
        $price = intval(Registry::getConfig()->getRequestParameter('price'));
        $mintime = intval(Registry::getConfig()->getRequestParameter('mintime'));
        $maxtime = intval(Registry::getConfig()->getRequestParameter('maxtime'));
        $path = '/units/' . $unit . '/';
        $data = array(
            "amount" => $amount,
            "listing_price" => $price,
            "delivery_time_min" => $mintime,
            "delivery_time_max" => $maxtime
        );
        $result = $this->_oApi->patchRequest($path,$data);
        print_r($result);
        die();
    }

    /**
     * return ShippingGroups
     */
    public function getShippingGroups(){
        $path = '/shipping-groups/seller/';
        return $this->_oApi->getRequest($path);
    }

}