<?php


namespace ewald\real\Application\Controller\Admin;


use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Request;

class real_returns extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController
{

    protected $_sClass = 'real_returns';

    protected $_sThisTemplate = 'real_returns.tpl';

    protected $_aViewData = null;

    protected $_oDb = null;

    protected $_sSecretKey = null;

    protected $_sClientKey = null;

    protected $_oApi = null;

    protected $_test = null;

    public function __construct()
    {
        $this->_oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $this->_oApi = new real_api();
        $this->_getInitialReturns();
    }


    public function render()
    {
        parent::render();
        return $this->_sThisTemplate;
    }

    public function getToken()
    {
        $req = new Request();
        return $req->getRequestEscapedParameter('stoken');
        //return Registry::getConfig()->getRequestParameter('stoken');
    }

    public function getAdminSid()
    {
        $req = new Request();
        return $req->getRequestEscapedParameter('force_admin_sid');
        //return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    protected function _getInitialReturns(){


        $time = time();
        $fromDate = date("Y-m-d", strtotime("-30 days", $time));
        $path = '/returns/seller/?ts_created:from=' . $fromDate . '&sort=ts_created:desc';
        $result = $this->_oApi->getRequest($path);

        $i = 0;

        foreach($result as $key){
            $this->_aViewData['returns'][$i] = new \stdClass();
            $this->_aViewData['returns'][$i]->id_return = $key['id_return'];
            $this->_aViewData['returns'][$i]->ts_created = $key['ts_created'];
            $this->_aViewData['returns'][$i]->ts_updated = $key['ts_updated'];
            $this->_aViewData['returns'][$i]->tracking_provider = $key['tracking_provider'];
            $this->_aViewData['returns'][$i]->tracking_code = $key['tracking_code'];
            $this->_aViewData['returns'][$i]->status = $key['status'];
            $i++;
        }

        print_r(json_encode($result));
        die();
    }

    public function getReturn(){
        $api = new real_api();
        $req = new Request();
        $id = $req->getRequestEscapedParameter('id');
        $path = '/returns/' . $id . '/?embedded=units,buyer' ;

        $return = $api->getRequest($path);


        // TODO: Buyer??
        //$idBuyer = $return['buyer']['id_buyer'];
        foreach($return['units'] as $unit ){
            $idReturnUnit = $unit['id_return_unit'];
            $idOrderUnit = $unit['id_order_unit']; // Muss eigentlich bei jedem Durchlauf gleich bleiben, da es sich um eine Retoure handelt
        }


        // LoadOrder


        // LoadReturnUnits

    }

    public function acceptReturn(){
        $path = '/return-units';
        //$this->_test = 'TEST';
        return $this->_lollipop();
    }

    public function rejectReturn(){

    }

    public function repairReturn(){

    }

    public function clarifyReturn(){

    }

    protected function _lollipop(){
        return $this->_test;
    }

}