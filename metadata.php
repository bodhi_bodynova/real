<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'                    => 'real',
    'title'                 => [
        'de'                => '<img src="../modules/ewald/real/out/img/favicon.ico" title="wtf">odynova Modul zur Real-Verwaltung'
    ],
    'description'           => [
        'de'                => 'Modul für die Verwaltung der real.de-Schnittstelle'
    ],
    'thumbnail'             => 'out/img/logo_bodynova.png',
    'version'               => '1.0',
    'author'                => 'Bodynova GmbH',
    'url'                   => 'https://bodynova.de',
    'email'                 => 'c.ewald@bodynova.de',

    'extend'                => [
    ],
    'controllers'           => array(
        'real_api'          => \ewald\real\Application\Controller\Admin\real_api::class,
        'real_articles'     => \ewald\real\Application\Controller\Admin\real_articles::class,
        'real_orders'       => \ewald\real\Application\Controller\Admin\real_orders::class,
        'real_offers'       => \ewald\real\Application\Controller\Admin\real_offers::class,
        'real_returns'      => \ewald\real\Application\Controller\Admin\real_returns::class
    ),
    'templates'             => array(
        'real_articles.tpl' => 'ewald/real/Application/views/admin/tpl/real_articles.tpl',
        'real_orders.tpl'   => 'ewald/real/Application/views/admin/tpl/real_orders.tpl',
        'real_offers.tpl'   => 'ewald/real/Application/views/admin/tpl/real_offers.tpl',
        'real_returns.tpl'  => 'ewald/real/Application/views/admin/tpl/real_returns.tpl'
    ),
    'blocks'                => [
        array(
            'template'      => 'headitem.tpl',
            'block'         =>'admin_headitem_inccss',
            'file'          =>'Application/views/admin/blocks/real_admin_headitem_inccss.tpl'
        ),
        array(
            'template'      => 'headitem.tpl',
            'block'         =>'admin_headitem_incjs',
            'file'          =>'Application/views/admin/blocks/real_admin_headitem_incjs.tpl'
        )
    ],
    'settings'              => [
        [
            'group'         => 'real_keys',
            'name'          => 'sRealClientKey',
            'type'          => 'str',
            'value'         => ''
        ],
        [
            'group'         => 'real_keys',
            'name'          => 'sRealSecretKey',
            'type'          => 'str',
            'value'         => ''
        ],
    ],
    'events'                => [
    ],
];